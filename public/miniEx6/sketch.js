//empty array - ammount in array is equal to how many bubbles i want in the for loop
let bubbles = [];
let gun;

function preload() {
  gun = loadSound('sound/ak.mp3');

}

function setup() {
createCanvas(windowWidth, windowHeight);
cursor(CROSS);
for(let i = 0; i < 15; i++) {
// for giving them different locations and sizes
let x = random(150,1000);
let y = random(150,500);
let r = random(10,50);
// making the spawn position of each bubble different
let b = new Bubble(x,y,r);
bubbles.push(b);
// push equals adding something to the end of the array, which in this case is a new bubble
//When pressed on button --> run setup running the for loop --> Adding more targets
var button1 = createButton('Add more targets');
button1.position(25, 25);
button1.mousePressed(setup);


}
}

  // bubbles.length is equal to length of array
function mousePressed(){
  for (let i = 0; i < bubbles.length; i++) {
    if (bubbles[i].clicked(mouseX,mouseY)) {
      bubbles.splice(i,1);
      gun.play();

      // splice removes something from the array
      // when mouse is being pressed, it checks if being pressed on ellipse - if so remove it from array

}

}
}
function draw() {
  background(255);
    for(let i = 0; i < bubbles.length; i++){
      if (bubbles[i].clicked(mouseX,mouseY)) {
      }
bubbles[i].move();
bubbles[i].show();


}
}

// creating our class
class Bubble {
  constructor(x,y,r) {
    this.x = x;
    this.y = y;
    this.r = r;

  }
clicked(lel1,lel2){
// if the distance of the curser is larger than the radius of the circle, then the cursor must be on the object
let d = dist(lel1,lel2, this.x, this.y);
if(d < this.r){
  return true;
} else {
  return false;

}


}

// the moving aspect of the object
move() {
    this.x = this.x + random(-5,5);
    this.y = this.y + random(-5,5);
  }

// how do i want the object to look?
show() {
stroke(184,15,10);
strokeWeight(10);
fill(255);
ellipse(this.x,this.y,this.r*2);


  }
}
