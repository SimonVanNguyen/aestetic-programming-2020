## Group Ideas - made with Pernille, Torvald & Anne 

Torvald: https://gitlab.com/Teepeek 

Anne: https://gitlab.com/AnneNielsen 

Pernille: https://gitlab.com/pernwn

Idea 1 

<img alt="YAP" src="https://i.imgur.com/S5SRd7O.png"> 


Idea 2 

<img alt="YAP" src="https://i.imgur.com/pkiJkRR.png"> 




## Individual flowchart of miniEx9 - made with the entire group

Link to project: https://gitlab.com/SimonVanNguyen/aestetic-programming-2020/-/tree/master/public/miniEx9

<img alt="YAP" src="https://i.imgur.com/NKUrbtn.png"> 

What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure?
I'm not quite sure if I understand this question correctly, but I guess its about how it is challenging to explain a algorithmic procedure through flowchart and make it more accessable in some sense. The challenges in my instance with my own flowchart was to exclude some element of my program. It would be hard to include everything and still have the flowchart maintain its "flow" and sense, and thats what i believe is a challenge. Another challenge would be to try to sort of translate the program into the flowchart. You could argue that a flowchart is a visualization of a an algorhitm, and what defines visualization is the the way it makes the point clearer and more understandable. This visualization also includes trying to rename some of the functions and fundamental ideas of programming into everyday langauge which also can be quite a challenge. 

What are the technical challenges for the two ideas and how are you going to address them?
As far as technical challenges the first idea is heavily similar to a previous miniex that we did, so the challenge for this idea would be to program something that would, stick out and be different than the previous program. We really liked the idea of concept of that particular program, but couldn't come up with any ground breaking ideas that could articulate the concept in a acceptable way. So to sum up the technical complications would be to creative with the concept and experiement with new syntax, that could potentially underline the same concept and idea. In terms of the second idea, the technical challenges would probably be o actually program an sort of artifical intelligence and utilize machine learning - a theme that for me personally find exciting, but also quite hard to grasp, when in comes down to syntax, since I haven't experiemented a lot with machine learning. But that would also be that part that would motivate me. To challenge myself and learn a new skill in programming. In the end it is hard to tell what specific part would be a challenge when programming either of the final project ideas, since we haven't made them yet. As a designer/programmer you'll have to sort of adapt to the problems that come up, and you may have to change ur initial idea or concept as a consequence of this. This is also what is mentioned in the text by Ensmenger, Nathan. "The Multiple Meanings of a Flowchart.",2016, pp. 321-351., that programmers real use of a flowchart before creating a program would be very superficial and wouldn't be updated with the evolving software.   

What is the value of the individual and the group flowchart that you have produced?

For me creating a flowchart before a specific program, allowed me to idea generate in a wider sense. In was a good place to externalize my inital ideas and try linking them up. When first coming up with ideas it can be really messy and hard to explain - especially when trying to explain it to others without a sketch etc. In that sense flowcharts carries a value to tear an idea into smaller and more simple steps to make it more understandable and easier to percieve for others. This is also underlined in the text by Ensmenger, that the ideal way to use a flowchart when first introduced was to use it as medium between the programmer and the manager to understand each other and their vision and ideas. According to ensmenger the flowcharts has 2 main goals. Firsly as i mentioned before to make someone cler of the intentions and idea of the program and secondly for the programmer/designer himself to ensure that the program fulfills the requirements and performs as intended. Using flowcharts after a program has been made also carries a value. Looking at my previous programs, one would notice it is not the most complex or difficult ones to grasp, but when taking a look at more complex programs it is very useful to again split the program into pieces, and explain the programs through a flowchart.  


## Sources: 

Inspiration for final project idea 1: https://www.youtube.com/watch?v=s7wmiS2mSXY

Ensmenger, Nathan. "The Multiple Meanings of a Flowchart." Information & Culture: A Journal of History, vol. 51 no. 3, 2016, pp. 321-351.


