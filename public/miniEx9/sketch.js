var cocktails;
var api = 'https://www.thecocktaildb.com/api/json/v1/1/search.php?s=';
var apiKey = "&APPID=1";
var ingredients = [];

function setup() {
noCanvas();
var button = select('#Submit');
button.mousePressed(cocktailsAsk);
//costumization of the button
button.style("background");
button.style("border", "2");
button.style("font-family", "Miller", "serif");
button.style("font-size", "20px");
button.style("font-style","italic");
button.style("padding", "5");
button.style("color","#1c1c1b");
button.style("display","block");
button.style("margin","20px", "auto", "0");
button.style("cursor", "pointer");
button.style("transsition", ".3s");

//costumization of the input bar
input = select('#Cocktail');
input.style("font-size", "20px");
input.style("font-family", "Miller", "serif");
input.style("padding", "0");
input.style("font-style","italic");
}

function cocktailsAsk(){
  var url = api + input.value() + apiKey; //The "input.value" is referenced to from our index file
  loadJSON(url, gotData);
}

function gotData(data){
  var cocktails = data.drinks;

  //To clear the previous search and show the new shearch:
  for (var j = 0; j < ingredients.length; j++) {
    ingredients[j].hide();
  }
  ingredients = [];

  //"Loading" the cocktail list in a for loop:
  for(var i = 0; 1 < cocktails.length; i++){
    // To create the headlines:
    append(ingredients,createElement("h2", cocktails[i].strDrink));
    //costumization of the text
    ingredients[ingredients.length-1].style("font-family", "serif");
    ingredients[ingredients.length-1].style("text-decoration", "underline");

    //going through the for-loop showing every picture for every drink
    append(ingredients,createImg(cocktails[i].strDrinkThumb, "Cocktails"));
      //resizing the picture
      ingredients[ingredients.length-1].size(450, 450);

    //showing headline for ingredients
    append(ingredients,createP("Ingredients:", cocktails[i]));
      //costumization of the text
      ingredients[ingredients.length-1].style("font-weight", "bold");
      ingredients[ingredients.length-1].style("font-size", "20px");


      //if the string in the JSON file doesn't show null, then this program will show the ingredient
      if(cocktails[i].strIngredient1 != null) {
        append(ingredients, createP(cocktails[i].strMeasure1 + cocktails[i].strIngredient1));
      }

      if(cocktails[i].strIngredient2 != null) {
        append(ingredients, createP(cocktails[i].strMeasure2 + cocktails[i].strIngredient2));
      }

      if(cocktails[i].strIngredient3 != null) {
        append(ingredients, createP(cocktails[i].strMeasure3 + cocktails[i].strIngredient3));
      }

      if(cocktails[i].strIngredient4 != null) {
        append(ingredients, createP(cocktails[i].strMeasure4 + cocktails[i].strIngredient4));
      }

      if(cocktails[i].strIngredient5 != null) {
        append(ingredients, createP(cocktails[i].strMeasure5 + cocktails[i].strIngredient5));
      }

      if(cocktails[i].strIngredient6 != null) {
        append(ingredients, createP(cocktails[i].strMeasure6 + cocktails[i].strIngredient6));
      }

      if(cocktails[i].strIngredient7 != null) {
        append(ingredients, createP(cocktails[i].strMeasure7 + cocktails[i].strIngredient7));
      }

      if(cocktails[i].strIngredient8 != null) {
        append(ingredients, createP(cocktails[i].strMeasure8 + cocktails[i].strIngredient8));
      }

      if(cocktails[i].strIngredient9 != null) {
        append(ingredients, createP(cocktails[i].strMeasure9 + cocktails[i].strIngredient9));
      }

      if(cocktails[i].strIngredient10 != null) {
        append(ingredients, createP(cocktails[i].strMeasure10 + cocktails[i].strIngredient10));
      }

      if(cocktails[i].strIngredient11 != null) {
        append(ingredients, createP(cocktails[i].strMeasure11 + cocktails[i].strIngredient11));
      }

      if(cocktails[i].strIngredient12 != null) {
        append(ingredients, createP(cocktails[i].strMeasure12 + cocktails[i].strIngredient12));
      }

      if(cocktails[i].strIngredient13 != null) {
        append(ingredients, createP(cocktails[i].strMeasure13 + cocktails[i].strIngredient13));
      }

      if(cocktails[i].strIngredient14 != null) {
        append(ingredients, createP(cocktails[i].strMeasure14 + cocktails[i].strIngredient14));
      }

      if(cocktails[i].strIngredient15 != null) {
        append(ingredients, createP(cocktails[i].strMeasure15 + cocktails[i].strIngredient15));
      }

    //showing headline for instructions
    append(ingredients, createP("Instructions:", cocktails[i]));
    //costumization of the text
      ingredients[ingredients.length-1].style("font-weight", "bold");
      ingredients[ingredients.length-1].style("font-size", "20px");

    //going through the for-loop showing the instructions for each drink
    append(ingredients, createP(cocktails[i].strInstructions));
  }
}
