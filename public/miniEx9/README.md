## Group number 2
For the best experience, open our program in full screen :)
## Questions to think about (for the README):
### What is the program about? Which API have you used and why? How much do you understand this data?
For this week´s MiniEx we have in group 2 created a cocktail searching machine. We have created this program to make bartending more accessible at home during this corona crisis, where we all have to stay at home isolated and therefore can't go out for a drink with friends.
We wanted to create something fun to do while being at home and to put a smile on your face. This program is an attempt to lift the mood, and create the opportunity to make your own drinks at home, maybe invite your friends to “friday-bar”, a party or just sharing a drink together over Facetime or any other video sharing platform. 

We got the idea to create this program by finding an API on margarita cocktails on the webpage “TheCocktailDB.com”. We tried out the URL to access the API data in a web browser, and we then checked if the JSON file (the file the URL led us to) contained all the information we wanted in our program. We felt like this file contained all the information needed for creating drinks at home (including measurements, ingredients, instructions and pictures of the drinks), the JSON file also had a good structure and it was easy for us to find the information we needed. Another reason for us to choose this API, was that it was free, open source, and we didn't have to register or sign up to access the data. 
When we added the URL to our sketch in Atom, we realized that we could separate the URL, and make it possible to search an arbitrary drink through an input bar and search button. 
For making the button and the input bar, we made this in the index file. Following Daniel Shiffman (video 10.6), we wrote the input id and button id, which executed in the sketch by writing “var button/input = select (**the ID name**”)”.

One great thing about using an API to get to an online JSON file is that the program is set-up to receive certain information and can update that information automatically. This means that, if a new drink combination to create a cocktail is being made, the distributors of our data(“the owners”), can add that new drink to the API file and it will appear in our program.  

### Can you describe and reflect on your process of making this mini exercise in terms of acquiring, processing, using and representing data? 


We ran into some complications when trying to make the page reload every time you search for a new cocktail. Because we didn’t make a canvas, we couldn’t “just” draw a new canvas over the already existing ‘recipes’ on the page. It was in fact possible to load the new recipes, the problem was, that the new ones would appear at the bottom of the recipes which were already on the page. We didn’t want it to do this, but instead we wanted recipes already on the page to disappear and the new one would then be able to load at the top. 

We asked our instructor about this and she suggested making an array which would then hide all the stuff within the array, then the new recipes could then be executed. Furthermore we also had some complications with customizing our program in terms of both html & css (since this is a new way of coding for us). We ended up by experimenting a lot and trying different things out in order to find the best possible result. 
In our process of handling APIs and the process of acquiring them, it seems as if it is not only a process of simply getting and using data, but that it is a transactional process which sets many restrictions and boundaries for the consumer. 
In our case, for gaining the APIs and a developer key for the kind of cocktails and how they were listed, a lot of the API materials were free to access in terms of their more basic content. If one would like to access extra features and content (more complicated kinds of APIs) one would have to sign up and pay with an added fee or in their words, ‘support’ the API providers. The same applies for the API key depending on what the consumer’s purpose is with the usage of the data. We were able to access a “test key” which could be used on development or educational grounds. On the flipside, the consumer would again have to ‘support’ under the same rules or criteria as aforementioned, in order to gain a production API key which is to be used if what we have created with the data would be publicly released on an application store (TheCocktainDB, 2015). 

### How do platform providers sort the data and give you the selected data? What are the power-relations in the chosen APIs? What is the significance of APIs in digital culture?
In this weeks MiniEx we have experienced, that the distribution of data through an API is controlled by the provider, an aspect Eric Snodgrass and Winnie Soon also talk about in the text “API practices and paradigms: Exploring the protocological parameters of API´s as key facilitators of sociotechnical forms of exchange” from 2020. 
They mention Google as an example of a dominating provider that controls the service and its data. Google is one of many dominating providers, who is able to charge those who request data which surpasses an amount of which Google agrees with. It is explained that Google setup restrictions in order to control, monitor, track and regulate the data which is used and distributed (Snodgras & Soon, 2020, pp. 10-12), - which in turn makes data seem like an object of which has a high market value, that we buy, sell, distribute etc. 
But even though data is sold all around in this way, it never really can be owned, as it is a source which is supplied under such restrictions that take away the consumers freedom to total ownership of data unless they provide it themselves. 


### What would we want to know more about? Try to formulate a question in relation to web APIs or querying/parsing processes that you want to investigate further if you have more time.
We had issues with combining API keys and when using the API data, the json file and its relation to the data we have written in the sketch file. We wonder how we can entwine all these together and make them function in a more controlled state in order to make them show up properly in the program. When thinking about API we don’t necessarily link it with large societal changes and critical change in online behaviour. But is there a possible link between this? And is there more into this? Cause when thinking about it API is what connects the world with data and information, but the providers can set their own guidelines of what should be distributed and also at what cost. So how does an API affect the general user of the internet on a daily basis? 


**Sources**
API Source: https://www.thecocktaildb.com/api.php
E.g. for vodka: https://www.thecocktaildb.com/api/json/v1/1/search.php?s=vodka 
Found through:  https://rapidapi.com/collection/cool-apis → https://apilist.fun/api/the-cocktail-db 
Inspiration to style the button:
https://www.w3schools.com/js/js_htmldom_css.asp
https://www.bowmore.com/node/16 
Snodgrass, Eric, & Winnie Soon. "API practices and paradigms: Exploring the protocological parameters of APIs as key facilitators of sociotechnical forms of exchange." First Monday [Online], 24.2 (2019): n. pag. Web. 13 Jan. 2020 
Daniel Shiffman, Working with data, The Coding Train, available at https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r(watch 10.4-10.10)
Used to create the “header” of our page: https://www.youtube.com/watch?v=t8V0dfYseMI&t=93s 
Inspiration to styling: https://www.w3schools.com/ 

RUNME: https://simonvannguyen.gitlab.io/aestetic-programming-2020/miniEx9/
(In case my RUNME doesn't work, go to Pernille's miniEx9 )

Link to respository: https://gitlab.com/SimonVanNguyen/aestetic-programming-2020/-/tree/master/public/miniEx9

<img alt="YAP" src="https://i.imgur.com/eFjOkVk.png"> 