## Words_ 

A collaboration between Simon & Torvald

## README questions

### Provide a title of your work and a short description of the work.
When coming up with the idea of “words_” our main intentions were to create something with words. Not in the sense of writing poetry with words, but more as an artistic way of displaying words than just reading it out loud or displaying it on an white canvas. But with this in mind, it was actually harder than expected to come up with a usable and also programmable work. Additionally we’d have to find something that we could link from our program to our source code. 

The overall idea with the work was that words signify many things. Our work attempts to express the possible functions of words and what they can mean to us. We have implemented different parts of speech as a foundation for this piece, which are four parts that sit on a translucent box. The transparency of the box gives way to see words from different perspectives, which leads to our intentions with the work; that we wanted to express the way that words can be used in many ways, with an indifference to context or connection, as they can even hold a power in themselves. 

### Describe your program in terms of how it works, and what you have used and learnt?
Our program consists of a 3 dimensional space, where a transparent box is centralized and is rotating horizontally. Additionally 4 sides of the box are displaying text within 4 categories (nouns,adjectives, verbs & adverbs). In order to do so we have first preloaded both the text font and the json file. In order to add 3 dimensional space in a program, you have to include webgl, and for this the text has to be imported. In terms of the data that the text displays, it is not from the same file, but from an json file. In this json file we have put our collection “words” in four categories of randomized examples which we have pulled from a website which creates a list of random words in a chosen part of speech. In our program we have used nouns, adjectives, verbs and adverbs. Our intention was with this to gain a completely randomized list of words which were in categories given to us by the website. We wanted to create a piece which would give us very little influence in elements that were chosen, which in our case became random words.  

We have learnt to influence the framecount on a specific section of code which in our case are the words which all loop through the lists at a random interval within five percent of the framecount. We have also learnt to make use of performative syntax in the way that we have created a readable piece of code with a specific meaning. We have done this through variables to create the “readable code”, which is also interwoven with if-statements; while reading the code, it is up to the reader to make meaning out of this.

### Analyze and articulate your work:
Our piece of performative code suggests that it should be read with variation, given that it is written in long lines with a max of three words per line. It is intentionally done so the “poetry” can be read with stress or a style which emphasises every single word as the user reads them. According to Geoff Cox, Alex Mclean and Adrian Ward in “The Aesthetics of Generative Code”, poetry can in this way produce a specific meaning which in our case has been written with effort to be read in a specific way.  (Cox & McLean, p. 4-5b) When interpreting the notation of our code, it has been written with variables that force the program to interpret them even though they in reality don’t actually have that much of an influence on the core code. Instead of creating comments, we have set the code up as a primary notation through variables. It becomes an odd mix of primary and secondary notation here as it both can be understood by the human and by the computer in the way it has been written. (Cox et al., p. 22-24a) In our work, the intention is that the source code should be read alongside the executed program, which is mentioned by Cox et al. (p. 9-11b to understand its entirety, as the visual part of the runme program wouldn’t otherwise make much sense without predetermined comprehension of the written code that has a given meaning.

The performativity in our program in itself revolves a lot around the source code. The program in itself is not as appealing, when the audience hasn't had a glance at the source code. The source code plays a big part of understanding the concept and will allow the audience to reflect upon the work. By that the audience may grasp, interpret and perceive the work entirely differently than otherwise. By adding voices to our program it would further emphasise the point of words having more than meets the eye. Adding voices to pronounce each and every word would first and foremost underline that words could be pronounced in various different ways, and with different dialects and emotions. 

**Referencelist:**
inspiration:
http://collection.eliterature.org/1/works/howe_kaprinska__open_ended.html

Webpage of the random listed words:
https://www.randomlists.com/random-words

Other:
Geoff Cox, and Alex McLean. Speaking Code. Cambridge, Mass.: MIT Press, 2013a. 17-38. 

Geoff Cox, Alex McLean, and Adrian Ward. "The Aesthetics of Generative Code." Proc. of Generative Art. 2001b.



RUNME: https://simonvannguyen.gitlab.io/aestetic-programming-2020/miniEx8/

Link to repository: https://gitlab.com/SimonVanNguyen/aestetic-programming-2020/-/tree/master/public/miniEx8

<img alt="YAP" src="https://i.imgur.com/UOx2Uo6.png"> 