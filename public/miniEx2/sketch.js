function setup() {
  // put setup code here
  createCanvas(windowWidth,windowHeight);
}
  function draw() {
    noStroke();
    background(220);
    fill(76,153,0);
    ellipse(200,200,200,200);

    fill(255);
    ellipse(170,170,30,30);
    ellipse(230,170,30,30);

    beginShape();
    strokeWeight(10);

stroke(0);
    fill(32,32,32);

    line(190,150,145,135);

    line(210,150,260,135);
    endShape();

    strokeWeight(1)
    fill(255);
    ellipse(170,170,30,30);
    ellipse(230,170,30,30);

    fill(25,51,0);
    ellipse(176,170,15,15);
    ellipse(224,170,15,15);

noStroke();
    fill(51,102,0);
    ellipse(220,260,15,25);
    ellipse(200,250,17,23);
    ellipse(170,250,10,15);
    ellipse(180,270,20,9);
    ellipse(210,290,30,12);

stroke(0);
strokeWeight(4);
    line(250,240,150,230);

    line(200,100,200,70);
    line(180,102,150,80);
    line(210,100,230,70);

noStroke();
fill(255,235,59);
ellipse(600,200,200,200);
fill(255);
rect(550,200,100,70,10);

stroke(255);
strokeWeight(4);
line(500,200,555,210);
line(514,250,555,265);
line(700,200,600,220);
line(680,260,595,266);

stroke(0)
strokeWeight(10);
line(650,150,630,160);
line(550,150,570,160);

stroke(200,10,10);
line(500,130,700,300);
line(700,130,500,300);
  }
