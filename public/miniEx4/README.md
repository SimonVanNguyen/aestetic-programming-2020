**One click is all it takes**

Provide a title of your work and a short description of the work (within 1000 characters) as if you are submitting to the festival open call.

One click is all it takes is a program trying to aritculate and critizize the current state of the internet. By that I'm trying to critizize how a website is build upon. In many cases when entering a website or a program, one have to give in to the guidelines that the programers provide one with. For example in many online shopping stores such as Asos, Zalando etc. here a must in order to shop is the either link a google or facebook account or create a new account. Either you give in and do as they command you to or you can't shop. My program is trying the articulate this specific point. The button appears as the only element, which can be interacted with when entering the program. The user can't do anything else than to click that specifc button in order to continue. Afterwards the outcome in my program is a bit exaggerated as there appears many element trying to emphasize, how data is being gathered when first giving in to these obligations. When shopping online and creating these accounts the companies gain data upon the user. Which gender are they, in which country do they life etc. When entering the site again they can see which items you may or may not have clicked upon and then recommend similar items, via algorithms.   

Describe your program and what you have used and learnt.
My program consists of multiple element which i'm going to breakdown one by one. When opening the program a pink background appears along with a simple button with the text "In order to continue please allow to expose yourself", which can be clicked on. When clicking on the button it sets of the boolean switch, which makes multiple element appear. First of all a pair of eyes appear in the center. Here I used ellipses and mapped the x and y coordinates in order to make the eyes "follow" the curser and follow you. Furthermore a surveillance camera appears on the top right hand corner. Here I have used preload function to insert a png. The same method is used with the microphone, where I have found an image and used the preload function to insert it in the canvas. Lastly the program records both sound and video, by utilizing the capture command. The camera display so the speak appears on the top left corner.  All these element are put under if cctvar == 1, which is when the button is pressed upon. 

Articulate how your program and thinking address the theme 'capture all'.
What are the cultural implicatons of data capture?

In terms of how the program relates to the theme 'capture all', it is meant to empathize the data companies and programs gets from users. As mentioned earlier a lot of websites and programs set certain standards in order to proceed. An example of this can be cookies. Here you have to give in and accept this. The are in that way giving permission to the program to capture data about you. This could be how many times you may have clicked upon a certain categories or viewed certain pages. 

RUNME: https://simonvannguyen.gitlab.io/aestetic-programming-2020/miniEx4/

Link to repository: https://gitlab.com/SimonVanNguyen/aestetic-programming-2020/-/tree/master/public/miniEx4

<img alt="YAP" src="https://i.imgur.com/liHjVFE.png"> 

<img alt="YAP" src="https://i.imgur.com/Mtdu4rD.png"> 