Describe your throbber design, both conceptually and technically.

Technically my main throbber consist of an ellipse that both rotates and increases in size. The ellipse keeps increasing in size untill it reaches a certain width, where it then decreases in size. This process keeps going on as an ongoing loop. Furthermore there are also 4 digits displayed as a part of my throbber, which keeps displaying random numbers. Finally there is a png file of a viral meme (internet joke), which is a mocking sponge bob.  

Conceptually i would say that my throbber is mocking or embarrasing the whole idea of a throbber and loading. This can be seen through multiple aspect of my throbber design. First of all the infinite shifting digits, at first glance seems fine, but the more you look into it there more it bothers you. It is made to portray the idea of loading as something annoying. Additionally the ever shifting number symbolize a behind the throbber point of view. How different numbers constantly pop on and off in different orders impossible for ordinary people to grasp or understand. Another mocking element of my throbber is the shifting caps lock in loading and the sponge bob png. It is added to empathize the idea of how people perceive a throbber or when something is loading. The emotions they connect with it are often negative.


What is your sketch? What do you want to explore and/or express?
What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your process of coding)?
Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a ticket transaction, and consider what a throbber communicates, and/or hides? How might we characterise this icon differently?

My sketch is a sort of a provocative throbber. It is made with the intention to bring out the message what everyone thinks, but doesn't say. The frustations and the emotions connected with a running throbber have been the main focus here. The time-related functions i have used are frameRate and the whole idea of changing the variables in the ellipses etc. I belive that time is relative in terms of programming. By that i mean that time doesn't exist if something isn't moving or animating, and this is done through function(draw) and the fact that frameCount keeps increasing. In terms of programming i belive frames and the function (draw) in p5 is the main idea of time. The frame count keeps going up and that is what i would define as the time value of programming. As far as function(draw) goes it loops the code written over and over again, to display something. Here values such as for example the radiance of ellipse can be set to change over time (frames). In terms of the what a throbber hides, is basicly all the loading procress for the program. A throbber in itself doesn't do anything else, than letting the user know, that the program running is currently loading. It it made to hide all the ongoing data being transmitted all hidden within user friendlines of the throbber. Thats why so many people get frustrated when a throbber appears, because they actually don't know what have caused the throbber to occour. I wouldn't say we can connect the throbber icon with other emotions than negative ones.      









RUNME: https://simonvannguyen.gitlab.io/aestetic-programming-2020/miniEx3/

Repository: https://gitlab.com/SimonVanNguyen/aestetic-programming-2020/-/tree/master/public/miniEx3

<img alt="YAP" src="https://i.imgur.com/Z7uNBUe.png"> 