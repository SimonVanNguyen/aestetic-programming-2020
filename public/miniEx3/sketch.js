
// defining variables
let a = 0;
let b = 20;
let speed = 10;
let img;

// arrays consisting of number values
var clock1 = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
var clock2 = ['0', '3', '2', '1', '4', '5', '8', '7', '6', '9']
var clock3 = ['1', '0', '2', '3', '5', '4', '6', '7', '8', '9']
var clock4 = ['9', '1', '4', '3', '1', '5', '6', '7', '8', '0']

function preload(){
  img = loadImage('pics/svampe.png');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
}


function draw() {
  image(img,50,50,200,100);

  push();
  background(255, 182, 193);
  translate(width / 2, height / 2);
  rotate(a);
  noStroke();
  fill(152, 251, 152);
  ellipse(0,0,b,30);

  ellipse(0,0)

frameRate(30);
  a = a + 1;


// if ellipse reaches width/20 it will decrease in size
// if ellipse reaches size of 20 pixels it will continue
if( b > width/20){
speed = -10;
} else if(b == 20){
speed = 10;
}
b = b + speed;

pop();


// setting new origin point
translate(width / 2, height / 2);


// random digital numbers
var  ur=random(clock1)

text(ur, -60, 40)

var  ur=random(clock2)

text(':',0,40);

text(ur, 30,40)

var  ur=random(clock3)
text(ur, -30,40)

var  ur=random(clock4)
text(ur, 60,40)


// for loading text
push();
textSize(40);
stroke(0);
fill(255,150);
text('LoAdInG',-70,100);
pop();

// for loading image
image(img,80,30,200,100);

}
