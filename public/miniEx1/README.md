How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?

So far i think that its quite a challenge while on the other hand it also makes a lot of sense. Coding seems very logic when i eventually make it work or somebody else helps write a code, but when i struggle with it, it seems as like nothing makes sense. On the other hand my first impression of it is very positive. It makes more sense now, when i take a look on a code and it doesn't seem as alien to me anymore. I feel that i have learned the root and basics of coding.

How is the coding process different from, or similar to, reading and writing text?
In some ways i believe that it is similar and on the other hand different. The same because coding is also a language, that can be taught, expressed and written. So what is even the difference? To me nothing more than just it is less considered something to write and read from the public.

What does code and programming mean to you, and how do the assigned readings help you to reflect on programming?

Coding and programming means that i can express myself in a new way. If i get an idea i can try to visualize it through coding in almost the same sense as expressing myself through drawing or playing music. Furthermore the assigned readings (so far) helps me to reflect on how you can possibly use programming, and also why even use it. Coding also means that i potentially code and program mock-ups and prototypes for my design ideas.

Link to repository: https://gitlab.com/SimonVanNguyen/aestetic-programming-2020/-/tree/master/public/miniEx1

RUNME: https://simonvannguyen.gitlab.io/aestetic-programming-2020/miniEx1/

<img alt="YAP" src="https://i.imgur.com/krd5nRS.png">
