// rule 1 - every pixel must be unique
// rule 2 - the prodominant nuance in the picture should always be purple  

function setup() {
createCanvas(windowWidth, windowHeight);
pixelDensity(1);
// setting up 1 pixel is actualy equal to 1 pixel

fill(255, 182, 193);
stroke(255, 182, 193);
strokeWeight(4);
rect(550,450,150, 55, 20);

textSize(20);
stroke(0);
fill(255);
text('Generate Art',566,485);
}

function draw() {

updatePixels();

}

function mousePressed() {
 if(mouseX > 550 && mouseX < 700 && mouseY > 450 && mouseY < 510) {

   background(51);
   loadPixels();
   for (var y = 0; y < height; y++) {
       for(var x = 0; x  < width; x++) {
     // checks every x after checking every y - nested for loop
     //all pixels are in a 1d array in the order (red, green, blue & alpha)
         var index = (x+y*width)*4;
         // for calculating the excact index/numerical value of each argument in the array
         pixels[index+0] = y;
         pixels[index+1] = random(110,150)
         pixels[index+2] = x;
         pixels[index+3] = 175;

}
}
}
}
