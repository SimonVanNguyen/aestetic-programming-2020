What are the rules in your generative program and describe how your program performs over time and how are the running of the rules contingently enabled emergent behaviors?

The two rules I have chosen in my program are that there should always be a heavy overweight of a pinkish nuance in the canvas and that every pixel in in the program must be unique. In terms of how my programs runs over time, there is not really a different state in the program other than, the program changes its look when the button "Generate art" is being pressed. My main idea was that the state of the program would constantly change in function draw, but i didn't quite manage to do that, since I ended up adding the generative art in the function mousePressed, which resulted in a static picture. This was due to the requirement of a conditional statement, that I had to fit in my if mousePressed function. The program would then have constantly shift the value of the fill of the pixel and it would in a bigger sense be related to or be dependant of time than now.
 
What's the role of rules and processes in your work?
The running of the rules is apparent and contingently when the artwork is being made by the program. The first rule is being fulfilled by the for loop and index value of the array. Every time that the for loop runs through a pixel a specific RGB and alpha combination is given to that pixel, resulting in every pixel being unique. As far as the other rule goes, that the pink color should be dominant is being fulfilled by adding a random range for the green value. By combining this with the numeric index value (red, blue and alpha), it results into a large portion of the canvas being pink. 

Draw upon the assigned reading(s), how does this mini-exercise help you to understand auto generator (e.g control, autonomy, instructions/rules)? Do you have any further thoughts about the theme of this chapter?
If we try to reflect upon the assigned reading my program fully underlines the statement of randomness and control. That something cannot be fully random. There has to be set some boundaries. This also relates to the term pseudo randomness. The idea of “fake” randomness, cause even though the computer randomly chooses for example a number between 1 and 100, there is always some kind of pattern related to the result. There is an algorithm behind telling it to choose a random number from this range, and there has been a person behind defining what random is in the specific programming language chosen. I believe that the whole idea of random and autonomy has something to do with what we strive for as human. We as humans are drawn to excitement, danger and the unknown and that goes for everything, in that matter I also think that the idea of random in programming allows space to experiment and “play”. I believe that playing with the unknown is an essential part of discovering and learning new things which goes for both in life but especially also in coding. 





RUNME: https://simonvannguyen.gitlab.io/aestetic-programming-2020/miniEx7/

Link to respository: https://gitlab.com/SimonVanNguyen/aestetic-programming-2020/-/tree/master/public/miniEx7

<img alt="YAP" src="https://i.imgur.com/DULhF7B.png"> 